package StrTask;

@FunctionalInterface
public interface Printable {
    void print();
}
