package StrTask;

import java.util.regex.*;

public class StringCreator {
    public static boolean checkFirstCapitalAndEndPeriod(String str) {
        Pattern pattern = Pattern.compile("[A-Z].*\\.");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    public static String replaceAll(String str) {
        return str.replaceAll("[AEOIUaeoiu]", "_");
    }
}