package StrTask;

import java.util.*;

public class Menu {
    private Map <String , String>  menu;
    private Map <String , Printable> menuMethods;
    private static Scanner input = new Scanner(System.in);
    Locale locale;
    ResourceBundle resourceBundle;

    public Menu(){
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("Menu",locale);
        setMenu();
        menuMethods = new LinkedHashMap<>();

        menuMethods.put("1", this::testStringUtils);
        menuMethods.put("2", this::testRegEx);
        menuMethods.put("3", this::internationalizeMenuEnglish);
        menuMethods.put("4", this::internationalizeMenuUkrainian);
        menuMethods.put("5", this::internationalizeMenuSpanish);
        menuMethods.put("Q", this::exit);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("Q", resourceBundle.getString("Q"));
    }

    private void testStringUtils() {
        StringUtils utils = new StringUtils();
        String name = "Doghru Ferid";
        int age = 20;
        String city = "Lviv";
        StringUtils stringUtils = new StringUtils();
        System.out.println(stringUtils.getStringObjects(name,age,city));
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuSpanish() {
        locale = new Locale("es");
        resourceBundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }
    private void testRegEx() {
        String str1 = "Meow meow meow";
        String str2 = "As always";
        String str3 = "sad and mad";
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);

        System.out.println("\nReplace all vowels");
        System.out.println(StringCreator.replaceAll(str1));
        System.out.println(StringCreator.replaceAll(str2));
        System.out.println(StringCreator.replaceAll(str3));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }
    
    private void exit(){
        System.out.println("Exit");
        System.exit(0);
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
